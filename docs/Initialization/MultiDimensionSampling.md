# Multiple Dimension Sampling

| function                  | description |
| :------------------------ | :---------- |
| initialisation_random     |             |
| initialisation_latin      |             |
| initialisation_anisotrope |             |

We will note *m* the number of points to be distributed in a *n* dimensions space.

## Random

![random-2](images/random-2.png)
![random-5](images/random-5.png)
![random-8](images/random-8.png)

![random-10](images/random-10.png)
![random-25](images/random-25.png)
![random-32](images/random-32.png)

## Latin Hypecube Sampling

* [Latin Hypercube Sampling, from Wikipedia](https://en.wikipedia.org/wiki/Latin_hypercube_sampling)

![latin-2](images/latin-2.png)
![latin-5](images/latin-5.png)
![latin-8](images/latin-8.png)

![latin-10](images/latin-10.png)
![latin-25](images/latin-25.png)
![latin-32](images/latin-32.png)

## Anisotrope HD LHS

![anisotrope-2](images/anisotrope-2.png)
![anisotrope-5](images/anisotrope-5.png)
![anisotrope-8](images/anisotrope-8.png)

![anisotrope-10](images/anisotrope-10.png)
![anisotrope-25](images/anisotrope-25.png)
![anisotrope-32](images/anisotrope-32.png)
