# Single Dimension Sampling

It is essential before allocating our population into a *n* dimensions space,
to be able to make a distribution in only one dimension.

We will note *m* the number of points to be distributed in this space.
And *x* the indicator of the point to be positioned on our axis.

*k* defines the interval where the point *x* will be placed.

## Centered

$$
k_x^m = [\frac{x-1}{m};\frac{x}{m}]
$$

$m=1$, ![centered-1](images/centered-1.png)
$=2$, ![centered-2](images/centered-2.png)

$m=3$, ![centered-3](images/centered-3.png)

$m=4$, ![centered-4](images/centered-4.png)

$m=5$, ![centered-5](images/centered-5.png)

## Balanced

$$
k_x^m = [\frac{2x-3}{2(m-1)};\frac{2x-1}{2(m-1)}], k_1^1 = [0;1]
$$

avec

$$
k_1^m[x] = 0, k_m^m[y]=1
$$

$m=1$, ![balanced-1](images/balanced-1.png)

$m=2$, ![balanced-2](images/balanced-2.png)

$m=3$, ![balanced-3](images/balanced-3.png)

$m=4$, ![balanced-4](images/balanced-4.png)

$m=5$, ![balanced-5](images/balanced-5.png)

## Spreaded

$$
k_x^m = [\frac{2x-3}{2(m-1)};\frac{2x-1}{2(m-1)}], k_1^1 = [-0.25;1.25]
$$

$m=1$, ![spreaded-1](images/spreaded-1.png)

$m=2$, ![spreaded-2](images/spreaded-2.png)

$m=3$, ![spreaded-3](images/spreaded-3.png)

$m=4$, ![spreaded-4](images/spreaded-4.png)

$m=5$, ![spreaded-5](images/spreaded-5.png)
