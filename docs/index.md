# Home

```mermaid
graph LR
I(Initialization) --> P(Processing)
P --> S(Selection)
S --> C(Crossover)
C --> M(Mutation)
M --> D{Termination}
D --> P
D--> E(End)
```
