#!/usr/bin/perl

use strict;
use warnings;
use Math::Trig;

# Levy03
# Xi [-10, 10]; f(Xi)=0 for Xi=1

my @parameters = split /:/, $ARGV[0];
my $result;

my $y = 1 + ($parameters[0]-1)/4;
$result = sin(pi*$y)**2;
for ( my $i = 0 ; $i < $#parameters ; $i++ ) {
        $y = 1 + ($parameters[$i]-1)/4;
        my $ymin = 1 + ( $parameters[$i-1] - 1 )/4;
        my $ymax = 1 + ( $parameters[$#parameters] - 1 )/4;
        $result += (($y-1)**2) * (1+10*sin(pi*$ymin)**2) + ($ymax-1)**2;
}

print $result;
