#!/usr/bin/perl

use strict;
use warnings;
use Math::Trig;

# Schwefel26
# Xi [-500, 500]; f(Xi)=0 for Xi=420.968746

my @parameters = split /:/, $ARGV[0];
my $result;

for ( my $i = 0 ; $i <= $#parameters ; $i++ ) {
        $result += $parameters[$i]*sin(sqrt(abs($parameters[$i])));
}

print $result;
