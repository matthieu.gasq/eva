#!/usr/bin/perl

use strict;
use warnings;

# Styblinski Tang
# Xi [-5, 5]; f(Xi)=39.16616570377142 for Xi=-2.903534018185960

my @parameters = split /:/, $ARGV[0];
my $result;

for ( my $i = 0 ; $i <= $#parameters ; $i++ ) {
        $result += ($parameters[$i] ** 4) - 16*($parameters[$i]**2) + 5*$parameters[$i];
}

$result = 0.5* $result;
print $result;
