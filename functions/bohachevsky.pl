#!/usr/bin/perl

use strict;
use warnings;
use Math::Trig;

# Bohachevsky
# Xi [-15, 15]; f(Xi)=0 for Xi=0 

my @parameters = split /:/, $ARGV[0];
my $result;

for ( my $i = 0 ; $i < $#parameters ; $i++ ) {
        $result += $parameters[$i]**2+2*$parameters[$i+1]**2-0.3*cos(3*pi*$parameters[$i])-0.4*cos(4*pi*$parameters[$i+1])+0.7;
}

print $result;
