#!/usr/bin/perl

use strict;
use warnings;

# Plateau
# XI [-5.12, 5.12]; f(Xi)=0 for Xi=0

my @parameters = split /:/, $ARGV[0];
my $result;

for ( my $i = 0 ; $i <= $#parameters ; $i++ ) {
        $result += abs(int($parameters[$i]));
}

print $result;
