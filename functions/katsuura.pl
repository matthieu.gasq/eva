#!/usr/bin/perl

use strict;
use warnings;

# Katsuura
# Xi [0, 100]; f(Xi)=1 for Xi=0;

my @parameters = split /:/, $ARGV[0];
my $result;

for ( my $i = 0 ; $i <= $#parameters ; $i++ ) {
        $result += $parameters[$i] ** 2;
}

print $result;
