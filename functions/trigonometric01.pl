#!/usr/bin/perl

use strict;
use warnings;
use Math::Trig;

# Trigonometric01
# Xi [0, pi]; f(Xi)=0 for Xi=0

my @parameters = split /:/, $ARGV[0];
my $result;

for ( my $i = 0 ; $i <= $#parameters ; $i++ ) {
        my $intern = 0;
        for ( my $j = 0 ; $j <= $#parameters ; $j++ ) {
                $intern += cos($parameters[$j]) + ($i+1)*(1 - cos($parameters[$i]) - sin($parameters[$i]));
        }
        $result += (($#parameters+1) - $intern)**2;
}

print $result;
