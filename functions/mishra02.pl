#!/usr/bin/perl
#
use strict;
use warnings;

# Mishra02
# Xi [0, 1]; f(Xi)=2 for Xi=1

my @parameters = split /:/, $ARGV[0];
my $result;

for ( my $i = 0 ; $i <= ($#parameters-1) ; $i++ ) {
        $result += ($parameters[$i]+$parameters[$i+1])/2;
}

$result = ($#parameters+1) - $result;
$result = (1+$result)**$result;

print $result;
