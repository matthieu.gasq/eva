#!/usr/bin/perl

use strict;
use warnings;

# Quintic
# Xi [-10, 10]; f(Xi)=0 for Xi=-1

my @parameters = split /:/, $ARGV[0];
my $result;

for ( my $i = 0 ; $i <= $#parameters ; $i++ ) {
        $result += abs($parameters[$i]**5-3*$parameters[$i]**4+4*$parameters[$i]**3+2*$parameters[$i]**2-10*$parameters[$i]-4);
}

print $result;
