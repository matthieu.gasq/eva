#!/usr/bin/perl

use strict;
use warnings;

# Cigar
# Xi [-100, 100]; f(Xi)=0 for Xi=0

my @parameters = split /:/, $ARGV[0];
my ($first, $result);
my $second = 0;
$first = $parameters[0]**2;

for ( my $i = 1 ; $i <= $#parameters ; $i++ ) {
         $second += $parameters[$i]**2;
}

$result = $first + 1e6*$second;

print $result;
