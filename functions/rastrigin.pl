#!/usr/bin/perl

use strict;
use warnings;
use Math::Trig;

# Rastrigin
# Xi [-5.12, 5.12]; f(Xi)=0 for Xi=0

my @parameters = split /:/, $ARGV[0];
my $result;

for ( my $i = 0 ; $i <= $#parameters ; $i++ ) {
        $result += ($parameters[$i] ** 2) - 10*cos(2*pi*$parameters[$i]);
}

$result = 10*($#parameters+1)+$result;
print $result;
