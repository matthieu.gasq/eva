#!/usr/bin/perl
# aptitude install libgd-gd2-perl
# ./MultiDimensionSampling.pl

use strict;
use warnings;

use lib './';
use GD;
use Initialization::MultiDimensionSampling;

my %settings;
$settings{dnaCode} = 200;
$settings{dnaSize} = 2;
my @population = qw/2 5 8 10 25 32/;
my $algo = $ARGV[0];

foreach my $p (@population) {
    $settings{population} = $p;
    {my @listPopulation;
    if ($algo eq "random"){@listPopulation = sampling_random(\%settings)}
    elsif ($algo eq "latin"){@listPopulation = sampling_latin(\%settings)}
    elsif ($algo eq "anisotrope"){@listPopulation = sampling_anisotrope(\%settings)}
    else{print "[ERROR] Wrong method Name\n"; exit}
    my $im = new GD::Image(200,200);
    my $white = $im->colorAllocate(255,255,255);
    my $black = $im->colorAllocate(0,0,0);
    $im->rectangle(0,0,199,199,$black); 
    my $color;
    if ($algo eq "random"){$color = $im->colorAllocate(0,175,0)}
    elsif ($algo eq "latin"){$color = $im->colorAllocate(0,0,255)}
    elsif ($algo eq "anisotrope"){$color = $im->colorAllocate(255,0,0)}

    for(my $i=0; $i <= $#listPopulation; $i++) {
        my ($x, $y) = split/:/, $listPopulation[$i]{dna};
        $im->arc($x,$y,5,5,0,360,$color);
    }
    open (my $file, ">", "$algo-$settings{population}.png")
        or die "Can't open < $algo-$settings{population}.png: $!";
    print $file $im->png;
    close ($file);
    }
}
