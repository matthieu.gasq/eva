#!/usr/bin/perl
# aptitude install libgd-gd2-perl

use strict;
use warnings;

use lib './';
use GD;
use Initialization::MultiDimensionSampling;
use Selection::Process;
use Display::Extended;

my %settings;
my $population = 10;
my $algo = 'random';

$settings{population} = $population;
$settings{dnaCode} = 200;
$settings{dnaSize} = 2;
$settings{function} = 'alpine02.pl';

my @inputs;

$inputs[0]{limInf} = 'true';
$inputs[0]{valInf} = 0;
$inputs[0]{valSup} = 10;
$inputs[0]{limSup} = 'true';
$inputs[0]{mutation} = 2;
$inputs[0]{correlation} = 'NULL';

$inputs[1]{limInf} = 'true';
$inputs[1]{valInf} = 0;
$inputs[1]{valSup} = 10;
$inputs[1]{limSup} = 'true';
$inputs[1]{mutation} = 2;
$inputs[1]{correlation} = 'NULL';

my @people = sampling_random(\%settings);

my $im = new GD::Image(200,200);
my $white = $im->colorAllocate(255,255,255);
my $black = $im->colorAllocate(0,0,0);

$im->rectangle(0,0,199,199,$black); 

my $color = $im->colorAllocate(0,175,0);

for(my $i=0; $i <= $#people; $i++) {
    my ($x, $y) = split/:/, $people[$i]{dna};
    $im->arc($x,$y,5,5,0,360,$color);
}
open (my $file, ">", "$algo-$population.png")
    or die "Can't open < $algo-$population.png: $!";
print $file $im->png;
close ($file);

@people = process_fitness(@people);

print display_fitness(@people);
