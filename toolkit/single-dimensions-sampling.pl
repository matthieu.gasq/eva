#!/usr/bin/perl
# aptitude install libgd-gd2-perl
# ./SingleDimensionSampling.pl

use strict;
use warnings;

use lib './';
use GD;
use Initialization::SingleDimensionSampling;

my $method = $ARGV[0];

my %settings;
$settings{dnaCode} = 400;
$settings{dnaSize} = 2;

for (my $i=1; $i <=5; $i++) {
    print "$method: [$i]\n";
    $settings{population} = $i;
    my @balanced;
    if($method eq 'centered') {@balanced = distribution_centered( \%settings )}
    elsif($method eq 'balanced') {@balanced = distribution_balanced( \%settings )}
    elsif($method eq 'spreaded') {@balanced = distribution_spreaded( \%settings )}
    else{print "[ERROR] Wrong method Name\n"; exit}
    my $im = new GD::Image(850,20);
    my $white = $im->colorAllocate(255,255,255);
    my $black = $im->colorAllocate(0,0,0);
    my $green = $im->colorAllocate(0,200,0);
    $im->dashedLine(225,10,625,10,$black);

    foreach my $value (@balanced) {
        my ($min,$max) = split /:/, $value;
        my $center = $min + ($max-$min)/2;
        $center +=225;
        $min += 225;
        $max += 225;
        print "[$min -> $center -> $max]\n";
        $im->arc($center,10,5,5,0,360,$green);
        $im->line($min,6,$min,14,$green);
        $im->line($max,6,$max,14,$green);
    }
    open (my $file, ">", "$method-$i.png")
        or die "Can't open < $method-$i.png: $!";
    print $file $im->png;
    close ($file);
}
