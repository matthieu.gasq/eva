package Display::Basic;

use strict;
use warnings;
use Exporter 'import';
our $VERSION = '1.00';
our @EXPORT  = qw(help_message opt_header);

sub help_message {
	my %s = %{$_[0]};
        print <<EOF;
Required Options:
        --dna-structure -d (string) ex: true:0:1:true_false:0:10000:true
        --function -f (string) ex: ./function.sh || /usr/local/bin/function.sh
Help:
        --help
EOF
        exit;
}

sub opt_header {
	my %s = %{$_[0]};
	my $result = "
################################
DNA Structure: $s{dnaStructure}
DNA Size: $s{dnaSize}
Function: $s{function}

Population: $s{population}
Fertility: $s{fertility}
################################
";
	return $result;
}
