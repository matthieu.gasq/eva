package Display::Extended;

use strict;
use warnings;
use Exporter 'import';
our $VERSION = '1.00';
our @EXPORT  = qw(display_performance display_fitness child_counter display_fitest display_people display_mutation display_diversity);

# Child Counter
sub child_counter {
        my $child_nb = 0;
        for ( my $i = 0 ; $i <= $#_ ; $i++ ) {
                if($_[$i]{performance} eq 'NULL'){
                        $child_nb++;
                }
        }
        return $child_nb;
}

sub display_people {
    my ( $p, $v, $s ) = @_;
    my @p = @{$p};
    my @v = @{$v};
    my %s = %{$s};
    
    my $result = "";
    
    for ( my $i = 0 ; $i <= $#p ; $i++ ) {
        my @space = split /:/, $p[$i]{dna};
        for ( my $j = 0 ; $j <= $#space ; $j++ ) {
                $space[$j] =
                  ( ( $v[$j]{valSup} - $v[$j]{valInf} ) *
                      ( $space[$j] / $s{dnaCode} ) ) + $v[$j]{valInf};
            }
        my $d = join ':', @space;
        $result .= "$d => P[$p[$i]{performance}] D[$p[$i]{diversity}>] $p[$i]{fitness}\n";
    }
    return $result;
}

sub display_performance {
    my $result = "";

    for ( my $i = 0 ; $i <= $#_ ; $i++ ) {
        $result .= "$_[$i]{dna} => P[$_[$i]{performance}]\n";
    }
    return $result;
}

sub display_diversity {
    my $result = "";

    for ( my $i = 0 ; $i <= $#_ ; $i++ ) {
        $result .= "$_[$i]{dna} => D[$_[$i]{diversity}]\n";
    }
    return $result;
}

sub display_fitness {
    my $result = "";

    for ( my $i = 0 ; $i <= $#_ ; $i++ ) {
        $result .= "$_[$i]{dna} => F[$_[$i]{fitness}]\n";
    }
    return $result;
}


sub display_mutation {
    my @list;
    for ( my $i = 0 ; $i <= $#_ ; $i++ ) {
        push @list, $_[$i]{mutation};
    }
    my $result = join ':', @list;
    $result .= "\n";
    return $result;
}

sub display_fitest {
	my @list = qw / 0 -10000 /;
	for ( my $i = 0 ; $i <= $#_ ; $i++ ) {
        	if (($_[$i]{performance} ne 'NULL') && ($list[1] < $_[$i]{performance})){
                	$list[1] = $_[$i]{performance};
                	$list[0] = $_[$i]{dna};
        	}
	}
	return @list;
}
