package Initialization::SingleDimensionSampling;

use strict;
use warnings;
use Exporter 'import';
our $VERSION = '1.00';
our @EXPORT  = qw(distribution_centered distribution_balanced distribution_spreaded);

# Distribution: Centered
sub distribution_centered {
    my %s = %{$_[0]};
    my @result;
    
    for (my $i = 0; $i < $s{population}; $i++) {
        my $borderMin = int $s{dnaCode}*(($i+1)-1)/$s{population};
        my $borderMax = int $s{dnaCode}*($i+1)/$s{population};
        push @result, "$borderMin:$borderMax";        
    }
    return @result;
}

# Distribution: Balanced
sub distribution_balanced {
    my %s = %{$_[0]};
    my @result;

    if ( $s{population} == 1 ) {
        my  $borderMin = int (0*$s{dnaCode});
        my $borderMax = int (1*$s{dnaCode});
        $result[0] = "$borderMin:$borderMax";
    }
    else {
        for (my $i = 0; $i < $s{population}; $i++) {
            my $borderMin = int $s{dnaCode}*(2*($i+1)-3)/(2*($s{population}-1));
            my $borderMax = int $s{dnaCode}*(2*($i+1)-1)/(2*($s{population}-1));
            if ($i == 0) {$borderMin = 0*$s{dnaCode}};
            if ($i == $s{population}-1){$borderMax = 1*$s{dnaCode}};
            push @result, "$borderMin:$borderMax";
        }
    }
    return @result;
}

# Distribution: Spreaded
sub distribution_spreaded {
    my %s = %{$_[0]};
    my @result;

    if ( $s{population} == 1 ) {
        my  $borderMin = int (-0.25*$s{dnaCode});
        my $borderMax = int (1.25*$s{dnaCode});
        $result[0] = "$borderMin:$borderMax";
    }
    else {
        for (my $i = 0; $i < $s{population}; $i++) {
            my $borderMin = int $s{dnaCode}*(2*($i+1)-3)/(2*($s{population}-1));
            my $borderMax = int $s{dnaCode}*(2*($i+1)-1)/(2*($s{population}-1));
            push @result, "$borderMin:$borderMax";
        }
    }
    return @result;
}
