package Initialization::Tools;

use strict;
use warnings;
use Exporter 'import';
our $VERSION = '1.00';
our @EXPORT  = qw(initialisation_discretization);
use Math::Random::OO::Normal;

sub initialisation_discretization {
    my ($min, $max) = @_;
    my $prng = Math::Random::OO::Normal->new();
    my $interval = $max - $min;
    my $value = int($min+($interval/2)) + int($prng->next()*($interval/8));
    return $value;
}
