package Initialization::MultiDimensionSampling;

use strict;
use warnings;
use Exporter 'import';
our $VERSION = '1.00';
our @EXPORT  = qw(sampling_random sampling_latin sampling_anisotrope);
use Algorithm::Combinatorics qw(variations_with_repetition);
use List::Util qw(shuffle);
use Initialization::SingleDimensionSampling;
use Initialization::Tools;

# Initialiation: Random
sub sampling_random {
        my (@result, @temp);
        my %s = %{$_[0]};
        for (my $i=0; $i < $s{population}; $i++) {
                for (my $j=0; $j < $s{dnaSize}; $j++) {
                $temp[$i][$j] = int(rand($s{dnaCode}));
                }
        $result[$i]{dna} = join ':', @{$temp[$i]};
        $result[$i]{performance} = 'NULL';
        $result[$i]{diversity} = 'NULL';
        $result[$i]{fitness} = 'NULL';
        }
        return @result;
}

# Initialisation: Latin Hypercube Sampling
sub sampling_latin {
        my %s = %{$_[0]};
        my @result;
    my @line = distribution_balanced(\%s);
    my @grid;

    for (my $i = 0; $i < $s{dnaSize}; $i++) {
        $grid[$i] = \@line;
    }

    for (my $i = 0; $i < $s{population}; $i++) {
        my @point;
        for (my $j = 0; $j < $s{dnaSize}; $j++) {
            my ($coord, $min, $max, $center, $pick);
            $pick = int rand ($#{$grid[$j]}+1);
            $coord = $grid[$j][$pick];
            ($min,$max) = split /:/, $coord;
            $center = initialisation_discretization($min, $max);
            push @point, $center;
            my @newline;
            @newline = @{$grid[$j]};
            splice @newline, $pick, 1;
            $grid[$j] = \@newline;
        }
        my $p = join ':', @point;
        $result[$i]{dna} = $p;
        $result[$i]{performance} = 'NULL';
        $result[$i]{diversity} = 'NULL';
        $result[$i]{fitness} = 'NULL';
    }
    return @result;
}

# Anisotrope HD LHS
sub sampling_anisotrope {
    my %s = %{$_[0]};
    my @result;
    my %param;
    $param{dnaCode}=$s{dnaCode};

    my $alpha = int $s{population}**(1/$s{dnaSize});
    my $beta = 1 + int $s{population}**(1/$s{dnaSize});

    my $nbBeta = -1;
    my $calc = 0;

    while ($calc < $s{population}) {
    	$nbBeta++;
        $calc = $beta**$nbBeta * $alpha**($s{dnaSize}-$nbBeta);
    }
    my $nbAlpha = $s{dnaSize}-$nbBeta;

   $param{population} = $alpha;
   my @lAlpha = distribution_balanced(\%param);

   $param{population} = $beta;
   my @lBeta = distribution_balanced(\%param);

   my @vBeta = variations_with_repetition(\@lBeta, $nbBeta);
   my @vAlpha = variations_with_repetition(\@lAlpha, $nbAlpha);

    my @long;
    for (my $i=0; $i <= $#vBeta; $i++) {
                my $joined = join ';', @{$vBeta[$i]};
                push @long, $joined;
    }

    my @short;
    for (my $i=0; $i <= $#vAlpha; $i++) {
                my $joined = join ';', @{$vAlpha[$i]};
                push @short, $joined;
    }
	
   my @list;
   foreach my $i (@long) {
	foreach my $j (@short) {
		if (($i ne "") and ($j ne "")){
		my $coord = "$i;$j";
		push @list, $coord;
		}
		elsif($i ne "") {push @list, $i}
		elsif($j ne "") {push @list, $j}
		}
	}

    my @solutions;
    foreach my $i (@list) {
	my @break = split/;/, $i;
	my @point;
	foreach my $j (@break) {
		my($min, $max) = split/:/, $j;
		my $center = initialisation_discretization($min, $max);
		push @point, $center;
		}
	my $value = join ':', @point;
	push @solutions, $value;
	}

        for (my $i=0; $i < $s{population}; $i++) {
                @solutions = shuffle(@solutions);
                my $selected = pop @solutions;
                $result[$i]{dna} = $selected;
                $result[$i]{performance} = 'NULL';
                $result[$i]{diversity} = 'NULL';
                $result[$i]{fitness} = 'NULL';


        }
        return @result;
}
