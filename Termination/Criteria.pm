package Termination::Criteria;

use strict;
use warnings;

use Exporter 'import';
our $VERSION = '1.00';
our @EXPORT  = qw(termination_loop termination_random termination_score termination_mutation termination_stdev termination_delta);

use List::Util qw(sum max min);
use Statistics::Basic qw(:all); #libstatistics-basic-perl

sub termination_score {
	my @score;
	my $diff = 1;
	my @dna = split /:/, $_[0]{dna};
	my $dnaSize = $#dna + 1;
	my $delta = 1.00e-5;

        for ( my $i = 0 ; $i <= $#_ ; $i++ ) {
	    if ($_[$i]{performance} ne 'NULL') {
            push @score, $_[$i]{performance};
		}
        }
        my $max = max @score;
	my $min = min @score;
	if (defined $max && defined $min) {
		$diff = $max - $min;
		$delta = $delta*(1+$min)/($dnaSize**2);
		if ($diff < $delta) {
			return 0;
		}
		else {
			return 1;
		}
	}
	else {
		return 1;
	}
}

sub termination_loop {
	my @life;
	for ( my $i = 0 ; $i <= $#_ ; $i++ ) {
	    push @life, $_[$i]{mutation};
	}
	my $max = max @life;
	return $max;
}

sub termination_random {
        for ( my $i = 0 ; $i <= $#_ ; $i++ ) {
	    if($_[$i]{correlation} eq 0) {
		$_[$i]{mutation} = 0;
	    }
        }
        return @_;
}

sub termination_mutation {
        my @life;
        for ( my $i = 0 ; $i <= $#_ ; $i++ ) {
            push @life, $_[$i]{mutation};
        }
        my $mutation_average = int sum(@life)/($#life+1);
        return $mutation_average;
}

sub termination_stdev {
        my @score;
	my $stdev;
        for ( my $i = 0 ; $i <= $#_ ; $i++ ) {
            if($_[$i]{performance} ne 'NULL'){
		my $value = 1.00e6*$_[$i]{performance};
		push @score, $value;
	}
        }
	if ($#score < 0) {
		$stdev = 1e9;	
	}
	else {
        	$stdev = stddev(@score);
	}
        return $stdev;
}

sub termination_delta {
        my ( $p, $sett ) = @_;
        my @p    = @{$p};
        my %sett = %{$sett};

        my @score;
        my $delta = 1.00e-6;

        for ( my $i = 0 ; $i <= $#p ; $i++ ) {
            if ($p[$i]{performance} ne 'NULL') {
            push @score, $p[$i]{performance};
                }
        }
        my $sum = sum @score;
        my $avg = $sum/($#score+1);

        if ($sett{lastavg} ne 'NULL') {
                if ( abs($avg-$sett{lastavg}) < $delta ) {
                        return 'exit';
                }
        }
        else {
                return $avg;
        }
}
