package Crossover::Combine;

use strict;
use warnings;

use List::Util qw(shuffle);
use Exporter 'import';
our $VERSION = '1.00';
our @EXPORT  = qw(quadratic_function linear_function random_balanced closest);

sub quadratic_function { # y = ax^2 + bx + c
    my ( $firstX, $firstY, $secondX, $secondY, $thirdX, $thirdY ) = @_;
    my ( $a, $b, $c );
    my $next;

    if ( ( $firstX - $thirdX ) * ( $secondX**2 - $firstX**2 ) +
        ( $secondX - $firstX ) * ( $thirdX**2 - $firstX**2 ) == 0 )
    {
        $a = 0;
    }
    else {
        $a =
          ( ( $secondY - $firstY ) * ( $firstX - $thirdX ) +
              ( $thirdY - $firstY ) * ( $secondX - $firstX ) ) /
          ( ( $firstX - $thirdX ) * ( $secondX**2 - $firstX**2 ) +
              ( $secondX - $firstX ) * ( $thirdX**2 - $firstX**2 ) );
    }
    if ( ( $secondX - $firstX ) == 0 ) {
        $b = $secondX;
    }
    else {
        $b =
          ( ( $secondY - $firstY ) - $a * ( $secondX**2 - $firstX**2 ) ) /
          ( $secondX - $firstX );
    }
    $c = $firstY - $a * $firstX**2 - $b * $firstY;

    # Quad Style
    if ( $a < 0 ) {
        $next = -$b / ( 2 * $a );
    }

    # Line Style
    else {
        if ( ( $firstY > $secondY ) && ( $firstY > $thirdY ) ) {
            $next = $firstX;
        }
        elsif ( ( $secondY > $firstY ) && ( $secondY > $thirdY ) ) {
            $next = $secondX;
        }
        else {
            $next = $thirdX;
        }

    }
    return $next;
}

sub random_balanced {
    my ( $first, $second ) = @_;
    if ( rand(2) < 1 )	{return $first;}
    else		{return $second;}
}

sub linear_function { # y = mx + b
    my ( $firstX, $firstY, $secondX, $secondY ) = @_;
    if ( $secondX == $firstX ) {
        return $secondX;
    }
    else {
        my $m = ( $secondY - $firstY ) / ( $secondX - $firstX );
        #my $b = $firstY - $m * $firstX;
        if ( $m > 0 ){
	    if($secondX > $firstX)	{return $secondX;}
	    else			{return $firstX;}
	}
	else {
            if($secondX > $firstX)      {return $firstX;}
            else                        {return $secondX;}
	}
    }
}

sub closest {
        my ( $p, $pic ) = @_;
        my @p = @{$p};
        my @distance;
        for (my $i=0; $i <= $#p; $i++) {
                my @d = split /:/, $p[$i]{dna};
                my @picked = split /:/, $p[$pic]{dna};
                my $result = 0;
                for (my $j=0; $j <= $#d; $j++) {
                        $result += ($picked[$j] - $d[$j])**2;
                }
                $result = sqrt($result);
                $p[$i]{distance} = $result;
                $distance[$i] = $result;
        }
        @p = sort {$a->{distance} <=> $b->{distance}} @p;
        return @p;
}
