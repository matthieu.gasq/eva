package Crossover::Pair;

use strict;
use warnings;

use List::Util qw(shuffle);
use Exporter 'import';
our $VERSION = '1.00';
our @EXPORT  = qw(crossover_double crossover_triple);

sub crossover_double {
    my ( $p, $v, $s ) = @_;
    my @p = @{$p};
    my @v = @{$v};
    my %s = %{$s};
    my @newPeople;

    my $actualPop = $#p;

    for ( my $i = 0 ; $i < $s{fertility} ; $i++ ) {
	my @choice  = shuffle(@p);
	#@choice = &closest(\@choice, 0);
        my $dParent = $choice[0]{dna};
        my $mParent = $choice[1]{dna};
        my @baby;
        my @dad = split /:/, $dParent;
        my @mom = split /:/, $mParent;
        for ( my $k = 0 ; $k <= $#dad ; $k++ ) {
            my $child_gen = random_balanced( $dad[$k], $mom[$k] );
            if ( ( $child_gen < 0 ) && ( $v[$k]{limInf} eq 'true' ) ) {
                $child_gen = -0.5 * $child_gen;
            }
            elsif (( $child_gen > $s{dnaCode} )
                && ( $v[$k]{limSup} eq 'true' ) )
            {
                $child_gen = $s{dnaCode} - 0.5 * ( $child_gen - $s{dnaCode} );
            }

            push @baby, $child_gen;
        }
        my $child = join( ':', @baby );
        my %entry = (
            dna         => $child,
            diversity   => 'NULL',
            performance => 'NULL',
            fitness     => 'NULL'
        );
        push @newPeople, \%entry;
    }
    push @p, @newPeople;
    return @p;
}

sub crossover_triple {
    my ( $p, $v, $s ) = @_;
    my @p = @{$p};
    my @v = @{$v};
    my %s = %{$s};
    my @newPeople;

    my $actualPop = $#p;

    for ( my $i = 0 ; $i < $s{fertility} ; $i++ ) {
        my @choice  = shuffle(@p);
	#@choice = &closest(\@choice, 0);
        my $fParent = $choice[0]{dna};    #First Parent
        my $sParent = $choice[1]{dna};    #Second Parent
        my $tParent = $choice[2]{dna};    #Third Parent
        my @baby;
        my @first  = split /:/, $fParent;
        my @second = split /:/, $sParent;
        my @third  = split /:/, $tParent;
        for ( my $k = 0 ; $k <= $#first ; $k++ ) {
            my $child_gen = quadratic_function(
                $first[$k], $choice[0]{performance}, $second[$k],
                $choice[1]{performance}, $third[$k], $choice[2]{performance}
            );
            if ( ( $child_gen < 0 ) && ( $v[$k]{limInf} eq 'true' ) ) {
                $child_gen = -0.5 * $child_gen;
            }
            elsif (( $child_gen > $s{dnaCode} )
                && ( $v[$k]{limSup} eq 'true' ) )
            {
                $child_gen = $s{dnaCode} -  0.5 * ( $child_gen - $s{dnaCode} );
            }

            push @baby, $child_gen;
        }
        my $child = join( ':', @baby );
        my %entry = (
            dna         => $child,
            diversity   => 'NULL',
            performance => 'NULL',
            fitness     => 'NULL'
        );
        push @newPeople, \%entry;
    }
    push @p, @newPeople;
    return @p;
}
