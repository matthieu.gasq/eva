package Mutation::Shift;

use strict;
use warnings;

use Exporter 'import';
our $VERSION = '1.00';
our @EXPORT  = qw(mutation_field mutation_correlated mutation_balanced);

use Statistics::Basic qw(:all); # aptitude install libstatistics-basic-perl

sub mutation_field {
	my ($p, $i) = @_;
	my @p = @{$p};
	my @i = @{$i};
	my @l;

	for (my $m = 0; $m <= $#p ; $m++) {
		my @d = split /:/, $p[$m]{dna};
		for (my $n = 0; $n <= $#d ; $n++) {
			$l[$n][$m] = $d[$n];	
		}
	}

	for (my $m = 0; $m <= $#i; $m++) {
		my $var = stddev(@{$l[$m]});		
		$i[$m]{mutation} = int (2*$var);
	}
	return @i;
}

sub mutation_correlated {
        my ($p, $v, $s) = @_;
        my @p = @{$p};
        my @v = @{$v};
        my %s = %{$s};

        for ( my $i = 0 ; $i <= $#p ; $i++ ) {
                if ( $p[$i]{performance} eq 'NULL' ) {
                        my @space = split /:/, $p[$i]{dna};
                        for (my $j = 0; $j <= $#space; $j++) {
				my $factor = rand((2*$v[$j]{correlation}**2)*$v[$j]{mutation});
				if ($v[$j]{correlation} < 0){
                                	$space[$j] = int( $space[$j] - $factor );
				}
				else {
					$space[$j] = int( $space[$j] + $factor );
				}
                                if ( ($space[$j] < 0) && ($v[$j]{limInf} eq 'true')) { $space[$j] = -$space[$j]/2 }
                                elsif ( ($space[$j] > $s{dnaCode}) && ($v[$j]{limSup} eq 'true') ) {
                                        $space[$j] = $s{dnaCode} - ( $space[$j] - $s{dnaCode} )/2;
                                }				
                        }
                        my $d = join ':', @space;
			$p[$i]{dna} = $d;
                }
        }
        return @p;
}

sub mutation_balanced {
        my ($p, $v, $s) = @_;
        my @p = @{$p};
        my @v = @{$v};
        my %s = %{$s};

        for ( my $i = 0 ; $i <= $#p ; $i++ ) {
                if ( $p[$i]{performance} eq 'NULL' ) {
                        my @space = split /:/, $p[$i]{dna};
                        for (my $j = 0; $j <= $#space; $j++) {
                                $space[$j] = int( $space[$j] + rand($v[$j]{mutation}) - ($v[$j]{mutation} / 2 ));
                                if ( ($space[$j] < 0) && ($v[$j]{limInf} eq 'true')) { $space[$j] = -$space[$j]/2 }
                                elsif ( ($space[$j] > $s{dnaCode}) && ($v[$j]{limSup} eq 'true') ) {
                                        $space[$j] = $s{dnaCode} - ( $space[$j] - $s{dnaCode} )/2;
                                }
                        }
                        my $d = join ':', @space;
                        $p[$i]{dna} = $d;
                }
        }
        return @p;
}
