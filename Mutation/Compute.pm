package Mutation::Compute;

use strict;
use warnings;

use Exporter 'import';
our $VERSION = '1.00';
our @EXPORT  = qw(mutation_factor);

use Statistics::Basic qw(:all); # aptitude install libstatistics-basic-perl

sub mutation_factor {
        my ($p, $i) = @_;
        my @p = @{$p};
        my @i = @{$i};
        my (@l,@s);

        # DNA
        for (my $m = 0; $m <= $#p ; $m++) {
                my @d = split /:/, $p[$m]{dna};
                for (my $n = 0; $n <= $#d ; $n++) {
                        $l[$n][$m] = $d[$n];
                }
        }

        # Score
        for (my $m = 0; $m <= $#p ; $m++) {
                $s[$m] = $p[$m]{performance};
        }

        for (my $m = 0; $m <= $#i; $m++) {
		#PRINTER
                my $corr = correlation(\@{$l[$m]}, \@s);
                if($corr eq 'n/a') {$corr = 0;}
                $i[$m]{correlation} = $corr;
        }
        return @i;
}
