package Selection::Tools;

use strict;
use warnings;

use Function;
use Math::Random::Discrete;    		# perl -MCPAN -e 'install Math::Random::Discrete'
use List::MoreUtils qw(firstidx);	# aptitude install liblist-moreutils-perl
use List::Util qw(sum min max shuffle);
use Parallel::ForkManager;		# aptitude install libparallel-forkmanager-perl
use Exporter 'import';
our $VERSION = '1.00';
our @EXPORT =
  qw(population_control);

# Population Control
sub population_control {
        my ( $p, $sett ) = @_;
        my @p    = @{$p};
        my %sett = %{$sett};

        my @score;

        for ( my $i = 0 ; $i <= $#p ; $i++ ) {
            if ($p[$i]{performance} ne 'NULL') {
            push @score, $p[$i]{performance};
                }
        }
    if ($#score == -1) {
        push @score, 1;
    }
        my $sum = sum @score;
        my $avg = $sum/($#score+1);
    my $factor = sqrt ($sett{dnaSize}*($sett{dnaCode}/2)**2);

        if ($sett{lastavg} ne 'NULL') {
        my $delta = abs (($avg-$sett{lastavg})/$factor);
        if ((1 <= abs $delta) || ($delta == 0)) {$delta = 1}
        else {$delta = 1 + int abs log $delta}
        $delta = int $delta/10;
        if ($delta == 0) {$delta=1}
        return ($delta, $avg);
    }
        else {
                return (int(log($sett{dnaCode})/10), $avg);
        }
}
