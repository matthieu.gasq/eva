package Selection::Select;

use strict;
use warnings;

use Function;
use Math::Random::Discrete;    		# perl -MCPAN -e 'install Math::Random::Discrete'
use List::MoreUtils qw(firstidx);	# aptitude install liblist-moreutils-perl
use List::Util qw(sum min max shuffle);
use Parallel::ForkManager;		# aptitude install libparallel-forkmanager-perl
use Exporter 'import';
our $VERSION = '1.00';
our @EXPORT = qw(select_normalized select_best);

# select: Proportional to Performance/Diversity/Fitness
sub select_normalized {
    my ( $p, $sett ) = @_;
    my @p    = @{$p};
    my %sett = %{$sett};
    my ( @s, @selectPeople );
    my $nbParent;

    for ( my $i = 0 ; $i <= $#p ; $i++ ) {
        push @s, $p[$i]{performance};
    }
    my $min = min @s;
    my $max = max @s;

    for ( my $i = 0 ; $i <= $#s ; $i++ ) {

        # BugShield
        if ( $max == $min ) { $max++; }
        $s[$i] = int( ( ( $s[$i] - $min ) / ( $max - $min ) ) * 100 );
    }

    if ($sett{population} > $#p) {$nbParent = $#p}
    else {$nbParent = $sett{population}}

    my $generator;
    for ( my $i = 0 ; $i < $nbParent; $i++ ) {
        $generator = Math::Random::Discrete->new( \@s, \@p );
        my $item = $generator->rand;
        my $index = firstidx { $_ eq $item } @p;
        $selectPeople[$i]{dna}         = $p[$index]{dna};
        $selectPeople[$i]{fitness}     = $p[$index]{fitness};
        $selectPeople[$i]{diversity}   = $p[$index]{diversity};
        $selectPeople[$i]{performance} = $p[$index]{performance};
        splice @p, $index, 1;
        splice @s, $index, 1;
    }
    return @selectPeople;
}

# select: The Best Ones
sub select_best {
    my ( $p, $sett ) = @_;
    my @p    = @{$p};
    my %sett = %{$sett};
    my @selectPeople;
    my $nbParent;

    if ($sett{population} > $#p) {$nbParent = $#p}
    else {$nbParent = $sett{population}}

    for ( my $i = 0 ; $i < $nbParent; $i++ ) {
        my $bestPerf  = -10000;
        my $bestIndex = 0;
        for ( my $j = 0 ; $j <= $#p ; $j++ ) {
            if (   ( $p[$j]{performance} ne 'NULL' )
                && ( $bestPerf < $p[$j]{performance} ) )
            {
                $bestIndex = $j;
                $bestPerf  = $p[$j]{performance};
            }
        }
        push @selectPeople, $p[$bestIndex];
        splice @p, $bestIndex, 1;
    }
    return @selectPeople;
}
