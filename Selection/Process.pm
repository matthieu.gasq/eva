package Selection::Process;

use strict;
use warnings;

use lib './';
use Math::Random::Discrete;    		# perl -MCPAN -e 'install Math::Random::Discrete'
use List::MoreUtils qw(firstidx);	# aptitude install liblist-moreutils-perl
use List::Util qw(sum min max shuffle);
use Parallel::ForkManager;		# aptitude install libparallel-forkmanager-perl
use Exporter 'import';
our $VERSION = '1.00';
our @EXPORT =
  qw(process_performance process_diversity process_fitness);

sub process_performance {
    my ( $p, $v, $s ) = @_;
    my @p = @{$p};
    my @v = @{$v};
    my %s = %{$s};

    my $pm = Parallel::ForkManager->new(8);

    $pm->run_on_finish(
        sub {
            my ( $pid, $exit_code, $ident, $exit_signal, $core_dump,
                $data_structure_reference )
              = @_;
            $p[ $data_structure_reference->{index} ]{performance} =
              $data_structure_reference->{result};
        }
    );

    for ( my $i = 0 ; $i <= $#p ; $i++ ) {
        if ( $p[$i]{performance} eq 'NULL' ) {
            my $pid = $pm->start and next;
            my @space = split /:/, $p[$i]{dna};
            for ( my $j = 0 ; $j <= $#space ; $j++ ) {
                $space[$j] =
                  ( ( $v[$j]{valSup} - $v[$j]{valInf} ) *
                      ( $space[$j] / $s{dnaCode} ) ) + $v[$j]{valInf};
            }
            my $d = join ':', @space;
            my $res = `perl ./functions/$s{function} $d`;

            # Reverse
            $res = -$res;
            $pm->finish( 0, { result => $res, index => $i } );
        }
    }
    $pm->wait_all_children;
    return @p;
}

sub process_diversity {
    my @ref;
    for ( my $i = 0 ; $i <= $#_ ; $i++ ) {
        my @dna = split ':', $_[$i]{dna};
        for ( my $j = 0 ; $j <= $#dna ; $j++ ) {
            $ref[$j][$i] = $dna[$j];
        }
    }
    for ( my $i = 0 ; $i <= $#ref ; $i++ ) {
        my $avg;
        for ( my $j = 0 ; $j <= $#{ $ref[$i] } ; $j++ ) {
            $avg += $ref[$i][$j];
        }
        $avg = $avg / ( $#{ $ref[$i] } + 1 );
        $ref[$i] = $avg;
    }
    for ( my $i = 0 ; $i <= $#_ ; $i++ ) {
        my $distance;
        my @dna = split ':', $_[$i]{dna};
        for ( my $j = 0 ; $j <= $#dna ; $j++ ) {
            $distance += abs( $dna[$j] - $ref[$j] );
        }
        $_[$i]{diversity} = int($distance);
    }
    return @_;
}

sub process_fitness {
    my ( @score, @diversity, @performance );
    for ( my $i = 0 ; $i <= $#_ ; $i++ ) {
        push @score,     $_[$i]{performance};
        push @diversity, $_[$i]{diversity};
    }

    my $minScore = min @score;
    my $maxScore = max @score;
    my $minDiver = min @diversity;
    my $maxDiver = max @diversity;

    for ( my $i = 0 ; $i <= $#_ ; $i++ ) {
        my ( $normScore, $normDiversity ) = ( 0, 0 );
        if ( $maxScore - $minScore != 0 ) {
            $normScore =
              int( 100 *
                  ( $_[$i]{performance} - $minScore ) /
                  ( $maxScore - $minScore ) );
        }
        if ( $maxDiver - $minDiver != 0 ) {
            $normDiversity =
              int( 25 *
                  ( $_[$i]{diversity} - $minDiver ) /
                  ( $maxDiver - $minDiver ) );
        }
        $_[$i]{fitness} = $normScore + $normDiversity;
    }
    return @_;
}
