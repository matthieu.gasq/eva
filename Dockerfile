FROM alpine:3.8
LABEL name="Alpine Based Docker Image + Perl Modules" version="1.0"
MAINTAINER Matthieu Gasq

ENV DEBIAN_FRONTEND noninteractive
ENV PERL5LIB src

RUN echo "@testing http://dl-4.alpinelinux.org/alpine/edge/testing/" >> /etc/apk/repositories

RUN apk --no-cache update && apk --no-cache add \
    curl \
    wget \
    tar \
    make \
    gcc \
    build-base \
    gnupg \
    perl \
    perl-dev \
    perl-list-moreutils \
    perl-sub-uplevel \
    perl-getopt-long \
    perl-gd \
    perl-parallel-forkmanager@testing \
    perl-number-format@testing

RUN curl -L http://xrl.us/cpanm > /bin/cpanm && chmod +x /bin/cpanm && \
    cpanm \
    Statistics::Basic \
    Algorithm::Combinatorics \
    Math::Trig \
    Math::Random::Discrete \
    Math::Random::OO::Normal

COPY ./Initialization /root/
COPY ./Selection /root/
COPY ./Crossover /root/
COPY ./Mutation /root/
COPY ./Termination /root/
COPY ./Display /root/
COPY ./toolkit /root/
